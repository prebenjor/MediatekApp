

# About "MediatekApp"  
Contains a simple database with misc media, such as books, movies, 
and school books used during my education linked to the semesters we used them.  

Might add more content further down the path.


## Dependencies

You need either MySQL or MariaDB database.  


## Directories

### dokumentasjon
Contains documentation. So far there are only the ERD for the database.
The `.graphml`-file is for [yEd](https://www.yworks.com/products/yed).
The `.png` contains an image of the ERD.  

### MariaDB directory

Contains the SQL part of the project. The database in itself is a MariaDB database.  
The `.json`-file is purely for testing so far.  

### Client directory for vuejs app

