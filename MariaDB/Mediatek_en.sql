DROP SCHEMA IF EXISTS mediatek;
CREATE SCHEMA mediatek;
USE mediatek;

DROP TABLE IF EXISTS Book;
DROP TABLE IF EXISTS Movie;
DROP TABLE IF EXISTS Magazine;
DROP TABLE IF EXISTS Course;
DROP TABLE IF EXISTS Semestertable;
DROP TABLE IF EXISTS ReadBook;
DROP TABLE IF EXISTS SchoolBook;
DROP TABLE IF EXISTS GradeLog;
DROP TABLE IF EXISTS ToBeAquired;

CREATE TABLE Book (
ISBN CHAR (13),
Titel VARCHAR (50),
Edition CHAR (3),
Author VARCHAR (70),
Publisher VARCHAR (70),
Pages CHAR (4),
Cover VARCHAR (10),
Genre VARCHAR (30),
CONSTRAINT BookPK PRIMARY KEY (ISBN)
);

CREATE TABLE Movie (
Title VARCHAR (50),
Ages CHAR (2),
Genre VARCHAR (30),
Medium VARCHAR (15),
CONSTRAINT MoviePK PRIMARY KEY (Title)
);

CREATE TABLE Magazine (
ISBN CHAR (13),
Title VARCHAR (50),
No CHAR (2),
Årgang YEAR,
CONSTRAINT MagazinePK PRIMARY KEY (ISBN, No, Årgang)
);

CREATE TABLE Course (
Coursecode CHAR(8),
Coursename CHAR(50),
Grade CHAR(1),
Studypoints DECIMAL(3,1),
CONSTRAINT CoursePK PRIMARY KEY (Coursecode)
);

CREATE TABLE Semestertable (
Semester CHAR(1),
Year CHAR(5),
CONSTRAINT SemestertablePK PRIMARY KEY (Semester)
);

CREATE TABLE ReadBook (
ISBN CHAR(13),
Started DATE,
Done DATE,
CONSTRAINT ReadBookPK PRIMARY KEY (ISBN, Done),
CONSTRAINT ReadBookBookFK FOREIGN KEY (ISBN) REFERENCES Book(ISBN)
);

CREATE TABLE SchoolBook (
ISBN CHAR(13),
Coursecode CHAR(8),
Semester CHAR(1),
CONSTRAINT SchoolbookPK PRIMARY KEY (ISBN, Coursecode),
CONSTRAINT SchoolbookBookFK FOREIGN KEY (ISBN) REFERENCES Book(ISBN),
CONSTRAINT SchoolbookCourseFK FOREIGN KEY (Coursecode) REFERENCES Course(Coursecode),
CONSTRAINT SchoolbookSemestertableFK FOREIGN KEY (Semester) REFERENCES Semestertable(Semester)
);

CREATE TABLE GradeLog (
Dato TIMESTAMP,
Coursecode CHAR(8),
OldGrade CHAR(1),
NewGrade CHAR(1),
CONSTRAINT GradeLogPK PRIMARY KEY (Dato, Coursecode),
CONSTRAINT GradeLogCourse FOREIGN KEY (Coursecode) REFERENCES Course(Coursecode)
);

-- Possible new table 

CREATE TABLE ToBeAquired (
Title VARCHAR(100),
Creatior VARCHAR(100),
Medium VARCHAR(15),
Aquired TINYINT,
CONSTRAINT ToBeAquired PRIMARY KEY (Title, Medium)
);
