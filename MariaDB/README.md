# Database related files

`Mediatek_en.sql` is not yet completly transelated.

## Mediatek(_en).sql (Creating the database)

Scripts for creating the database.
- `Mediatek.sql`
- `Mediatek_en.sql`
    - Same script as `Mediatek.sql`, but in English.

## visning.sql (VIEWS)

List of views:
- `Studiepoeng_totalt`
    - Show sum of study points
- `Semesteroversikt`
    - Show book titles, coursename, grades, semester (number and year)
- `Karakterutskrift`
    - Print grades
- `Karakterutskrift_antall`
    - Number of grades
- `Navn_tabeller`
    - Name of current tables
- `Alt_tmp`
    - Used for `Alt`
- `Alt`
    - Show information related to book, school and courses. "Everything".

## triggere.sql (PL SQL, triggers)

Contains two triggers, mostly for testing so far.  
The first trigger does no work, as triggers can't return a value. The second
trigger stores changes done to grades, and stores course code, the old grade, new grade and
when the change was made.
