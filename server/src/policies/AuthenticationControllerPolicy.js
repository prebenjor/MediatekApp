const Joi = require('joi')

module.exports = {
    register (req, res, next){
        const schema = {
            email: Joi.string().email(),
            password: Joi.string().regex(
                new RegExp('^[a-zA-z0-9]{8,32}$')
            )
        }

        const {error, value} = Joi.validate(req.body, schema)
        
        if (error){
            switch(error.details[0].context.key){
                case 'email':
                    res.status(400).send({
                        error: 'Du må skrive inn en gyldig e-postadresse'
                    })
                    break
                case 'password':
                    res.status(400).send({
                        error: `Passordet må bestå av følgende:
                        <br>
                        1. Kun små og store bokstaver og/eller tall.
                        <br>
                        2. Mellom 8 og 32 tegn.
                        `
                    })
                   
                    break
                default:
                    res.status(400).send({
                        error: 'Feil ved registrering, '
                    })

            }
        } else{
            next()
        }
        
    }
}