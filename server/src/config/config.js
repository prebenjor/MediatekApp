module.exports = {
    port: 8081,
    db: {
        database: process.env.DB_NAME || 'mediatracker',
        user: process.env.DB_USER || 'mediatracker',
        password: process.env.DB_PASS || 'mediatracker',
        options: {
            dialect: process.env.DIALECT || 'mysql',
            host: process.env.HOST || 'localhost',
            storage: './mediatracker.mysql'

        }
    }
}