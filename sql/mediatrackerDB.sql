CREATE DATABASE IF NOT EXISTS mediatracker;
CREATE USER 'mediatracker'@'localhost' IDENTIFIED BY 'mediatracker';
GRANT ALL PRIVILEGES ON mediatracker.* TO 'mediatracker'@'localhost';

use mediatracker;
select * from users;